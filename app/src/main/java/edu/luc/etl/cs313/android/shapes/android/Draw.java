package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;

import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME
		this.paint = paint; // FIXME
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		int i = paint.getColor();
		paint.setColor(c.getColor());

		if (c.getShape() instanceof Circle)
			onCircle((Circle) c.getShape());
		else if (c.getShape() instanceof Group)
			onGroup((Group) c.getShape());
		else if (c.getShape() instanceof Stroke)
			onStroke((Stroke) c.getShape());
		else if (c.getShape() instanceof Fill)
			onFill((Fill) c.getShape());
		else if (c.getShape() instanceof Location)
			onLocation((Location) c.getShape());
		else if (c.getShape() instanceof Outline)
			onOutline((Outline) c.getShape());
		else if (c.getShape() instanceof Polygon)
			onPolygon((Polygon) c.getShape());
		else if (c.getShape() instanceof Group)
			onGroup((Group) c.getShape());
		else
			onRectangle((Rectangle) c.getShape());
		paint.setColor(i);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		Style i = paint.getStyle();
		paint.setStyle(Style.FILL_AND_STROKE);

		if (f.getShape() instanceof Circle)
			onCircle((Circle) f.getShape());
		else if (f.getShape() instanceof Group)
			onGroup((Group) f.getShape());
		else if (f.getShape() instanceof Stroke)
			onStroke((Stroke) f.getShape());
		else if (f.getShape() instanceof Fill)
			onFill((Fill) f.getShape());
		else if (f.getShape() instanceof Location)
			onLocation((Location) f.getShape());
		else if (f.getShape() instanceof Outline)
			onOutline((Outline) f.getShape());
		else if (f.getShape() instanceof Polygon)
			onPolygon((Polygon) f.getShape());
		else if (f.getShape() instanceof Group)
			onGroup((Group) f.getShape());
		else
			onRectangle((Rectangle) f.getShape());
		paint.setStyle(i);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for (Shape a :g.getShapes() ) {
			if (a instanceof Circle)
				onCircle((Circle) a);
			else if (a instanceof Group)
				onGroup((Group) a);
			else if (a instanceof Stroke)
				onStroke((Stroke) a);
			else if (a instanceof Fill)
				onFill((Fill) a);
			else if (a instanceof Location)
				onLocation((Location) a);
			else if (a instanceof Outline)
				onOutline((Outline) a);
			else if (a instanceof Polygon)
				onPolygon((Polygon) a);
			else
				onRectangle((Rectangle) a);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(),l.getY());
		if (l.getShape() instanceof Circle)
			onCircle((Circle) l.getShape());
		else if (l.getShape() instanceof Group)
			onGroup((Group) l.getShape());
		else if (l.getShape() instanceof Stroke)
			onStroke((Stroke) l.getShape());
		else if (l.getShape() instanceof Fill)
			onFill((Fill) l.getShape());
		else if (l.getShape() instanceof Location)
			onLocation((Location) l.getShape());
		else if (l.getShape() instanceof Outline)
			onOutline((Outline) l.getShape());
		else if (l.getShape() instanceof Polygon)
			onPolygon((Polygon) l.getShape());
		else
			onRectangle((Rectangle)l.getShape());
		canvas.translate(l.getX()*-1,l.getY()*-1);

		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0,0,r.getWidth(),r.getHeight(),paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {

		Style i = paint.getStyle();
		paint.setStyle(Style.STROKE);

		if (o.getShape() instanceof Circle)
			onCircle((Circle) o.getShape());
		else if (o.getShape() instanceof Group)
			onGroup((Group) o.getShape());
		else if (o.getShape() instanceof Stroke)
			onStroke((Stroke) o.getShape());
		else if (o.getShape() instanceof Fill)
			onFill((Fill) o.getShape());
		else if (o.getShape() instanceof Location)
			onLocation((Location) o.getShape());
		else if (o.getShape() instanceof Outline)
			onOutline((Outline) o.getShape());
		else if (o.getShape() instanceof Polygon)
			onPolygon((Polygon) o.getShape());
		else if (o.getShape() instanceof Group)
			onGroup((Group) o.getShape());
		else
			onRectangle((Rectangle) o.getShape());
		paint.setStyle(i);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		int count = s.getPoints().size();
		float[] pts = new float[count*4];
		pts[0]=s.getPoints().get(0).getX();
		pts[1]=s.getPoints().get(0).getY();
		pts[(count*4)-2]=s.getPoints().get(0).getX();
		pts[(count*4)-1]=s.getPoints().get(0).getY();
		for (int i=0;i<(count-1);i++)
		{
			pts[(i*4)+2]=s.getPoints().get(i+1).getX();
			pts[(i*4)+3]=s.getPoints().get(i+1).getY();
			pts[(i*4)+4]=s.getPoints().get(i+1).getX();
			pts[(i*4)+5]=s.getPoints().get(i+1).getY();
		}

		canvas.drawLines(pts, paint);
		return null;
	}
}
