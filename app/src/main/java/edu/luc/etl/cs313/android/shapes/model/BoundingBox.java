package edu.luc.etl.cs313.android.shapes.model;

import android.graphics.Rect;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return new Location(0, 0,f.getShape());
	}

	@Override
	public Location onGroup(final Group g) {
		//final Location b = simpleOutline.accept(v);
		//final Rectangle r = (Rectangle) b.getShape();
		int rx ,ry; rx= ry =0;
		int x , y; x= y = Integer.MAX_VALUE;

		for (Shape a:g.getShapes() ) {

			Location i;
			if (a instanceof Circle)
				i = onCircle((Circle) a);
			else if (a instanceof Group)
				i =  onGroup((Group) a);
			else if (a instanceof Stroke)
				i =  onStroke((Stroke) a);
			else if (a instanceof Fill)
				i =  onFill((Fill) a);
			else if (a instanceof Location)
				i =  onLocation((Location) a);
			else if (a instanceof Outline)
				i =  onOutline((Outline) a);
			else if (a instanceof Polygon)
				i =  onPolygon((Polygon) a);
			else
				i= onRectangle((Rectangle)a);

			while(i.getShape() instanceof Stroke|| i.getShape() instanceof Fill ||i.getShape() instanceof Outline || i.getShape() instanceof Location|| i.getShape() instanceof Group)
			{
				 if (i.getShape() instanceof Stroke)
				i =  onStroke((Stroke) i.getShape());
			 if (i.getShape() instanceof Fill)
				i =  onFill((Fill) i.getShape());
			 if (i.getShape() instanceof Outline)
				i =  onOutline((Outline) i.getShape());
				if (i.getShape() instanceof Location)
					i =  onLocation((Location) i.getShape());
				if(i.getShape()instanceof Group)
					i=onGroup((Group)i.getShape());
			}

			if(i.getShape()instanceof Circle) {
				int r = ((Circle) i.getShape()).getRadius();
				if (x > i.getX()-r)
					x = i.getX()-r;
				if (x > i.getY()-r)
					y = i.getY()-r;
				if (rx < i.getX()+r)
					rx = i.getX()+r;
				if (ry < i.getY()+r)
					ry = i.getY()+r;
			}
//			else if(i.getShape()instanceof Group){
//
//				i = onGroup((Group) i.getShape());
//				int tx = ((Rectangle)i.getShape()).getWidth();
//				int ty = ((Rectangle)i.getShape()).getHeight();
//				if (x > i.getX())
//					x = i.getX();
//				if (x > i.getY())
//					y = i.getY();
//				if (rx < i.getX()+tx)
//					rx = i.getX()+tx;
//				if (ry < i.getY()+ty)
//					ry = i.getY()+ty;
//			}
			else {
				int tx = ((Rectangle)i.getShape()).getWidth();
				int ty = ((Rectangle)i.getShape()).getHeight();
				if (x > (i.getX()))
					x = i.getX();
				if (x > i.getY())
					y = i.getY();
				if (rx < i.getX()+tx)
					rx = i.getX()+tx;
				if (ry < i.getY()+ty)
					ry = i.getY()+ty;
			}

		}
		return  new Location(x,y,new Rectangle((rx-x),(ry-y)));
	}

	@Override
	public Location onLocation(final Location l) {

		return l;
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		return new Location(0, 0, r);
	}

	@Override
	public Location onStroke(final Stroke c) {
		return new Location(0, 0,c.getShape());
	}

	@Override
	public Location onOutline(final Outline o) {
		return new Location(0, 0,o.getShape());
	}

	@Override
	public Location onPolygon(final Polygon s) {
		int x = 0;
		int y = 0;
		int rx = 0;
		int ry = 0;
		if(s.getPoints().size()>0) {
			x = rx = s.getPoints().get(0).getX();
			y = ry = s.getPoints().get(0).getY();
			for (Point p : s.getPoints()
					) {
				if (x > p.getX()) {
					x=p.getX();
				}
				if (rx < p.getX()) {
					rx = p.getX();
				}
				if (y > p.getY()) {
					y= p.getY();
				}
				if (ry < p.getY()) {
					ry= p.getY();
				}
			}
		}
		return  new Location(x, y,new Rectangle((rx-x),(ry-y)));
	}
}
